package Modelo;

public class Docente {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private int horas;
    
    public Docente(){
        this.numDocente = 0;
        this.nombre = "";
        this.domicilio = "";
        this.nivel = 0;
        this.pagoBase = 0.0f;
        this.horas = 0;
    }

    public Docente(int numDocente, String nombre, String domicilio, int nivel, float pagoBase, int horas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.horas = horas;
    }
    
    public Docente(Docente docente){
        this.numDocente = docente.numDocente;
        this.nombre = docente.nombre;
        this.domicilio = docente.domicilio;
        this.nivel = docente.nivel;
        this.pagoBase = docente.pagoBase;
        this.horas = docente.horas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }
    
    public float calcularPago(){
        float calculo = 0.0f;
        switch(this.nivel){
            case 1:
                calculo = this.pagoBase + (this.pagoBase * 0.30f);
                break;
            case 2:
                calculo = this.pagoBase + (this.pagoBase * 0.50f);
                break;
            case 3:
                calculo = this.pagoBase + this.pagoBase;
                break;
        }
        return calculo * this.horas;
    }
    
    public float calcularImpuesto(){
        return this.calcularPago() * 0.16f;
    }
    
    public float calcularBono(int hijos){
        float bono = 0.0f;
        if(hijos >= 1 && hijos <= 2){
            bono = this.calcularPago() * 0.05f;
        }
        else if(hijos >= 3 && hijos <= 5){
            bono = this.calcularPago() * 0.10f;
        }
        else{
            bono = this.calcularPago() * 0.20f;
        }
        return bono;
    }
}