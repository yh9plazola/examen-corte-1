package Controlador;

import Modelo.Docente;
import Vista.dlgDocente;

import javax.swing.JOptionPane;
import javax.swing.JFrame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Controlador implements ActionListener{
    private Docente doc;
    private dlgDocente vista;
    
    public Controlador(dlgDocente vista, Docente doc){
        this.doc = doc;
        this.vista = vista;
        //Escuchar eventos
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnMostrar.addActionListener(this);
        this.vista.btnLimpiar.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnCerrar.addActionListener(this);
    }
    
    public static void main(String[] args){
        Docente obj = new Docente();
        dlgDocente vista = new dlgDocente(new JFrame(), true);
        Controlador controlador = new Controlador(vista, obj);
        controlador.inicializarVista();
    }
    
    public void inicializarVista(){
        this.vista.setTitle(":: Docente ::");
        this.vista.setSize(460, 525);
        this.vista.setLocationRelativeTo(null);
        this.vista.setVisible(true);
    }
    
    private void limpiar(){
        this.vista.txtNumDocente.setText("");
        this.vista.txtNombre.setText("");
        this.vista.txtDomicilio.setText("");
        this.vista.jSpNivel.setValue("Licenciatura");
        this.vista.txtPagoHora.setText("");
        this.vista.txtHoras.setText("");
        this.vista.jSpHijos.setValue(0);
        this.vista.lblPagoHoras.setText("0");
        this.vista.lblPagoBono.setText("0");
        this.vista.lblDescuento.setText("0");
        this.vista.lblTotal.setText("0");
    }
    
    private Boolean isVacio(){
        return this.vista.txtNumDocente.getText().equals("") ||
               this.vista.txtNombre.getText().equals("") ||
               this.vista.txtDomicilio.getText().equals("") ||
               this.vista.txtPagoHora.getText().equals("") ||
               this.vista.txtHoras.getText().equals("");
    }
    
    private Boolean isValido(){
        try
        {
            if(Integer.parseInt(this.vista.txtNumDocente.getText()) <= 0 ||
               Float.parseFloat(this.vista.txtPagoHora.getText()) <= 0.0f ||
               Integer.parseInt(this.vista.txtHoras.getText()) <= 0){
               JOptionPane.showMessageDialog(this.vista, "No puedes colocar numeros negativos", "Docente", JOptionPane.WARNING_MESSAGE);
               return false;
            }
        }
        catch(NumberFormatException ex)
        {
            JOptionPane.showMessageDialog(this.vista, "Ocurrio un error: " + ex.getMessage(), "Docente", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.vista.btnNuevo){
            this.vista.btnMostrar.setEnabled(true);
            this.vista.btnGuardar.setEnabled(true);
            this.vista.btnLimpiar.setEnabled(true);
            this.vista.btnCancelar.setEnabled(true);
            this.vista.btnCerrar.setEnabled(true);
            // JTextFields
            this.vista.txtNumDocente.setEnabled(true);
            this.vista.txtNombre.setEnabled(true);
            this.vista.txtDomicilio.setEnabled(true);
            this.vista.jSpNivel.setEnabled(true);
            this.vista.txtPagoHora.setEnabled(true);
            this.vista.txtHoras.setEnabled(true);
            this.vista.jSpHijos.setEnabled(true);
        }
        if(e.getSource() == this.vista.btnCerrar){
            if(JOptionPane.showConfirmDialog(this.vista, "¿Deseas salir?", "Title", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                this.vista.setVisible(false);
                this.vista.dispose();
                System.exit(0);
            }
        }
        if(e.getSource() == this.vista.btnGuardar){
            if(this.isVacio()){
                JOptionPane.showMessageDialog(this.vista, "No debe haber campos vacios", "Docente", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(!this.isValido()){
                return;
            }
            this.doc.setNumDocente(Integer.parseInt(this.vista.txtNumDocente.getText()));
            this.doc.setNombre(this.vista.txtNombre.getText());
            this.doc.setDomicilio(this.vista.txtDomicilio.getText());
            switch(this.vista.jSpNivel.getValue().toString()){
                case "Licenciatura":
                    this.doc.setNivel(1);
                    break;
                case "Maestria":
                    this.doc.setNivel(2);
                    break;
                case "Doctorado":
                    this.doc.setNivel(3);
                    break;
                    
            }
            this.doc.setPagoBase(Float.parseFloat(this.vista.txtPagoHora.getText()));
            this.doc.setHoras(Integer.parseInt(this.vista.txtHoras.getText()));
            JOptionPane.showMessageDialog(this.vista, "Se guardo con exito", "Docente", JOptionPane.INFORMATION_MESSAGE);
        }
        if(e.getSource() == this.vista.btnMostrar){
            if(Integer.parseInt(this.vista.jSpHijos.getValue().toString()) <= 0){
                JOptionPane.showMessageDialog(this.vista, "No puedes colocar numeros de hijos negativos o iguales a 0", "Docente", JOptionPane.WARNING_MESSAGE);
                return;
            }
            this.vista.txtNumDocente.setText(String.valueOf(this.doc.getNumDocente()));
            this.vista.txtNombre.setText(this.doc.getNombre());
            this.vista.txtDomicilio.setText(this.doc.getDomicilio());
            
            switch(this.doc.getNivel()){
                case 1:
                    this.vista.jSpNivel.setValue("Licenciatura");
                    break;
                case 2:
                    this.vista.jSpNivel.setValue("Maestria");
                    break;
                case 3:
                    this.vista.jSpNivel.setValue("Doctorado");
                    break;
            }
            this.vista.txtPagoHora.setText(String.valueOf(this.doc.getPagoBase()));
            this.vista.txtHoras.setText(String.valueOf(this.doc.getHoras()));
            this.vista.lblPagoHoras.setText(String.valueOf(this.doc.calcularPago()));
            this.vista.lblPagoBono.setText(String.valueOf(this.doc.calcularBono(Integer.parseInt(this.vista.jSpHijos.getValue().toString()))));
            this.vista.lblDescuento.setText(String.valueOf(this.doc.calcularImpuesto()));
            this.vista.lblTotal.setText(String.valueOf(this.doc.calcularPago() - this.doc.calcularImpuesto() + this.doc.calcularBono(Integer.parseInt(this.vista.jSpHijos.getValue().toString()))));
        }
        if(e.getSource() == this.vista.btnLimpiar){
            this.limpiar();
        }
        if(e.getSource() == this.vista.btnCancelar){
            this.limpiar();
            this.vista.btnMostrar.setEnabled(false);
            this.vista.btnGuardar.setEnabled(false);
            this.vista.btnLimpiar.setEnabled(false);
            this.vista.btnCancelar.setEnabled(false);
            this.vista.btnCerrar.setEnabled(false);
            // JTextFields
            this.vista.txtNumDocente.setEnabled(false);
            this.vista.txtNombre.setEnabled(false);
            this.vista.txtDomicilio.setEnabled(false);
            this.vista.jSpNivel.setEnabled(false);
            this.vista.txtPagoHora.setEnabled(false);
            this.vista.txtHoras.setEnabled(false);
            this.vista.jSpHijos.setEnabled(false);
        }
    }
}
